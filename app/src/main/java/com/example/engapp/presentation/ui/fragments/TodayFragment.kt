package com.example.engapp.presentation.ui.fragments

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProvider
import com.example.engapp.databinding.FragmentTodayBinding
import com.example.engapp.presentation.models.WordsModel
import com.example.engapp.presentation.ui.adapter.RecentWordsAdapter
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class TodayFragment : Fragment() {

    private lateinit var binding: FragmentTodayBinding
    private lateinit var viewModel: TodayViewModel
    private var wordsAdapter = RecentWordsAdapter(this::onItemClick)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTodayBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this).get(TodayViewModel::class.java)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
        observes()
        setupListener()
        currentDate()
    }

    private fun initialize() {
        binding.recycler.adapter = wordsAdapter
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun observes() {
        viewModel.getWordsList().observe(viewLifecycleOwner) {
            wordsAdapter.setList(it)
        }
    }

    private fun setupListener() = with(binding) {
        var bol = true
        val startColor = Color.parseColor("#13A5E8")
        val endColor = Color.parseColor("#3AB7E2")
        val gradientDrawable = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM,
            intArrayOf(startColor, endColor)
        )
        val whiteColor = Color.WHITE
        val blackColor = Color.BLACK
        btnTranslationOfTheQuotation.setOnClickListener {
            when (bol) {
                true -> {
                    clQuosten.background = gradientDrawable
                    tvDate.setTextColor(whiteColor)
                    tvQuotesOfTheDay.setTextColor(whiteColor)
                    tvQuoteAuthor.setTextColor(whiteColor)
                    btnTranslationOfTheQuotation.setTextColor(Color.GRAY)
                }
                false -> {
                    clQuosten.setBackgroundColor(whiteColor)
                    tvDate.setTextColor(blackColor)
                    tvQuotesOfTheDay.setTextColor(blackColor)
                    tvQuoteAuthor.setTextColor(blackColor)
                    btnTranslationOfTheQuotation.setTextColor(startColor)
                }
            }
            bol = !bol
        }
        btnTranslationOfTheIdiom.setOnClickListener {
            when (bol) {
                true -> {
                    clIdiom.background = gradientDrawable
                    tvIdiomOfTheDay.setTextColor(whiteColor)
                    tvMiniIdiom.setTextColor(whiteColor)
                    btnTranslationOfTheIdiom.setTextColor(Color.GRAY)
                }
                false -> {
                    clIdiom.setBackgroundColor(whiteColor)
                    tvIdiomOfTheDay.setTextColor(blackColor)
                    tvMiniIdiom.setTextColor(blackColor)
                    btnTranslationOfTheIdiom.setTextColor(startColor)
                }
            }
            bol = !bol
        }
    }
    private fun onItemClick(wordsModel: WordsModel) {

    }
    private fun currentDate() {
        val currentDate = Calendar.getInstance().time
        val dateFormat = SimpleDateFormat("d MMMM", Locale("ru"))
        val formattedDate = dateFormat.format(currentDate)
        binding.tvDate.text = formattedDate
    }
}
