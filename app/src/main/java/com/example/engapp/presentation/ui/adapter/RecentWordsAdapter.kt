package com.example.engapp.presentation.ui.adapter

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.engapp.databinding.ItemWordsBinding
import com.example.engapp.presentation.models.WordsModel

open class RecentWordsAdapter(
    protected val onItemClick : (wordsModel : WordsModel) -> Unit
) : RecyclerView.Adapter<RecentWordsAdapter.CarViewHolder>() {

    private var wordsList: List<WordsModel> = ArrayList()

    fun setList(list: List<WordsModel>) {
        this.wordsList = list
        notifyDataSetChanged()
    }

    inner class CarViewHolder(private val binding: ItemWordsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var bol = true
        private val startColor = Color.parseColor("#5AC4FD")
        private val endColor = Color.parseColor("#24ADF1")
        private val gradientDrawable = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM,
            intArrayOf(startColor, endColor)
        )
        init {
            itemView.setOnClickListener {
                onItemClick(wordsList[adapterPosition])
                binding.clItem.setBackgroundColor(Color.RED)
                when (bol) {
                    true -> {
                        binding.clItem.background = gradientDrawable
                        binding.tvWords.setTextColor(Color.WHITE)
                    }
                    false -> {
                        binding.clItem.setBackgroundColor(Color.WHITE)
                        binding.tvWords.setTextColor(Color.BLACK)
                    }
                }
                bol = !bol
            }
        }

        fun onBind(wordsModel: WordsModel) {
            binding.tvWords.text = wordsModel.words
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        return CarViewHolder(ItemWordsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int = wordsList.size

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        holder.onBind(wordsList[position])
    }
}
