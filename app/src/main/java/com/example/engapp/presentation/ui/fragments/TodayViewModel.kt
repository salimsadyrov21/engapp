package com.example.engapp.presentation.ui.fragments

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.engapp.presentation.models.WordsModel

class TodayViewModel : ViewModel() {

    private val wordsList: MutableLiveData<ArrayList<WordsModel>> = MutableLiveData()

    fun getWordsList(): MutableLiveData<ArrayList<WordsModel>> {
        val wordsArrayList: ArrayList<WordsModel> = ArrayList()

        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))
        wordsArrayList.add(WordsModel("Friendship"))

        wordsList.value = wordsArrayList
        return wordsList
    }
}